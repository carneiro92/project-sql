SELECT 
    user.first_name, user.last_name, address.street_number, address.street_name,
    address.city_name, address.zip_code, address.state_name, address.country_name,
    hosts.activity
FROM hosts
INNER JOIN hosts_has_address ON hosts_has_address.Address_idAddress = hosts.idHosts
INNER JOIN address ON hosts_has_address.Address_idAddress = address.idAddress
INNER JOIN user ON user.idUser = hosts.User_idUser;