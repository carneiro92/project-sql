CREATE DEFINER=`root`@`localhost` PROCEDURE `AddUserToWork`(
		IN userID INT,
        IN workID INT
)
BEGIN
	SELECT @personNB :=person_number, @voluntariesNB:= voluntaries, @skill := Skill_idSkill 
    FROM work WHERE idWork = workID;
    IF  @personNB < @voluntariesNB THEN
		INSERT INTO User_has_Work(User_idUser,Work_idWork,Work_Skill_idSkill,present) 
        VALUES (userID, workID, @skill, 1);
		UPDATE work SET person_number = @personNB + 1
		WHERE idWork = workID;
	ELSE
		SIGNAL SQLSTATE '99999' SET MESSAGE_TEXT = 'Max worker number';
    END IF; 
END