CREATE DEFINER=`root`@`localhost` PROCEDURE `addUpdateDeleteUser`(IN id INTEGER,
IN first_name varchar(120),
IN last_name varchar(120),
IN birth_date DATE,
IN registered_date DATETIME,
IN username VARCHAR(120),
IN password VARCHAR(255))
BEGIN
	INSERT INTO `woofing`.`user` (`idUser`,
`first_name`,
`last_name`,
`birth_date`,
`registered_date`,
`username`,
`password`)
		VALUES (id,first_name, last_name, birth_date, registered_date, username, password);
END;
