CREATE PROCEDURE `AddUser`(
	IN inFirstname VARCHAR(255),
    IN inLastname VARCHAR(255), 
    IN inBirthDate VARCHAR(10), 
    IN inRegisteredDate DATETIME, 
    IN inUsername VARCHAR(255),
	IN inPassword VARCHAR(255)
    )
BEGIN
                SET inPassword = SHA2(inPassword, 512);
                SET inUsername = SHA2(inUsername, 512);
                INSERT INTO user (first_name, last_name, birth_date, registered_date, username, Password)
                VALUES (inFirstname, inLastname, inBirthDate, inRegisteredDate, inUsername,  inPassword);
END