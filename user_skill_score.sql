CREATE VIEW `user_skils_score` AS
	SELECT concat(user.first_name,user.last_name) AS FullName, skill.title, score FROM user_has_skill 
    INNER JOIN user ON user.idUser = user_idUser
    INNER JOIN skill ON skill.idSkill = skill_idSkill