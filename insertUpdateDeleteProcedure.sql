CREATE DEFINER=`root`@`localhost` PROCEDURE `addUpdateDeleteUser`(IN id INTEGER,
IN first_name varchar(120),
IN last_name varchar(120),
IN birth_date DATE,
IN registered_date DATETIME,
IN username VARCHAR(120),
IN password VARCHAR(255),
IN statementType NVARCHAR(20))
BEGIN
		IF statementType <=> 'Insert'
			THEN BEGIN 
				INSERT INTO `woofing`.`user` (`idUser`,
`first_name`,
`last_name`,
`birth_date`,
`registered_date`,
`username`,
`password`)
                VALUES (id,first_name, last_name, birth_date, registered_date, username, password);
			END;

		IF StatementType <=> 'Select'  
        THEN BEGIN  
            SELECT *  
            FROM   `woofing`.`user`  
        END; 
  
		IF StatementType <=> 'Update'  
        THEN BEGIN  
            UPDATE `woofing`.`user`  
            SET    `first_name` = first_name,  
                   `last_name` = last_name,  
                   `birth_date` = birth_date,  
                   `registered` = registered,
                   `username` = username,
                   `password` = password
            WHERE  `idUser` = id;
        END;  
		IF StatementType <=> 'Delete'  
        THEN BEGIN  
            DELETE FROM `woofing`.`user`  
            WHERE  `idUser` = id; 
        END;
        END IF;
END IF;
END;
END IF;
END IF;
END